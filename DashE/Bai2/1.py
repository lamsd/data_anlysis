from dash import Dash, dcc, html, Input, Output, callback

app = Dash(__name__)

app.layout = html.Div([
    html.H6("Change the value in the text box to see callbacks in action!"),
    html.Div([
        "Input: ",
        dcc.Input(id='my-input', value='initial value', type='text')
    ]),
    html.Br(),
    html.Div(id='my-output'),
    html.Br(),
    html.Div(id='my-output1')
])

@callback(
    Output(component_id="my-output", component_property='children'),
    Input(component_id='my-input', component_property='value')
)
def update_d(input_value):
    return f"Output: {input_value}"

@callback(
    Output(component_id="my-output1", component_property='children'),
    Input(component_id='my-input', component_property='value')
)
def update_dA(input_value1):
    return f"Output: {input_value1}"



if __name__=="__main__":
    app.run_server(host="192.168.1.13", port=8050, debug=True)