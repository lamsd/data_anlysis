from dash import Dash, dcc, html, Input, callback, Output


app = Dash(__name__)

app.layout = html.Div([
    html.Div(children=[
        html.Label("Dropdown"),
        dcc.Dropdown(['New York City', 'Montréal', 'San Francisco'], 'Montréal'),
        html.Br(),
        html.Label('Multi-Select Dropdown'),
        dcc.Dropdown(['New York City', 'Montréal', 'San Francisco'],
                     ['Montréal', 'San Francisco'],
                     multi=True),
        html.Br(),
        html.Label('Radio Items'),
        dcc.RadioItems(['New York City', 'Montréal', 'San Francisco'], 'Montréal'),
    ], style={"padding":10, 'flex':1}),
    html.Div(children=[
        html.Label("Dropdown"),
        dcc.Dropdown(['New York City', 'Montréal', 'San Francisco'], 'Montréal'),
        html.Br(),
        html.Label('Text Input'),
        dcc.Input(value='MTL', type='text'),

        html.Br(),
        html.Label('Slider'),
        dcc.Slider(
            id="hello_data",
            min=0,
            max=9,
            marks={i: f'Label {i}' if i == 1 else str(i) for i in range(1, 6)},
            value=5,
        ),
    ], style={'padding': 10, 'flex': 1}),
    html.Div(id='updatemode-output-container', style={'margin-top': 20})
], style={'display': 'flex', 'flex-direction': 'row'})

@callback(Output("updatemode-output-container", 'children'),
          Input('hello_data', 'value'))

def display_value(value):
    print(value)
    return "Linear Value: {}".format(value)

if __name__ =="__main__":
    app.run_server(host="192.168.1.13", port=8050, debug=True)